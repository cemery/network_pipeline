#!/bin/bash
function display() {
    echo "Choose what you want to do :" >&2
    echo
    echo "		1. Basic Analysis: BA"
    echo "		2. Only Flexflux: F "
    echo "		3. Only Caspo: C "
    echo "		4. Display help : H "
    echo
    read -e -p "Choice:	" choice 

}

function display_help() {
    echo "Usage: menu.sh " >&2
    echo
    echo "Option available : "
    echo "   BA, --basic_analysis         run Flexflux and then caspo"
	echo "   F, --flexflux      run only FlexFlux"
    echo "   C, --caspo         run only Caspo"
    echo "   D, --display              Set on which display to host on "
    echo
    # echo some stuff here for the -a or --add-options 
    exit 1
}

function read_files_flexflux(){
	echo "Current location:"
	pwd
	# #Input
	echo "Give the following files: "
	echo 
	echo "Files for FlexFlux: "
	read -e -p "MetabolicNetwork (sbml):	" file_metabo 
	read -e -p "Constrainst (txt):		" file_constraints 
	read -e -p "RegulatoryNetwork(sbml):	" file_regu

}

function read_files_caspo(){
	echo ""
	echo "Files for Caspo use: "
	
	read -e -p "Prior knowledge network (.sif): " file_pkn
	read -e -p "Experimental setup (.json):	" file_setup
	read -e -p "Logical networks (dnf):		" file_dnf
	echo ""

}


function verifications_flexflux (){
	if [ -e "$file_metabo" ] && [ "${file_metabo##*.}" == "xml" ] || [ "${file_metabo##*.}" == "sbml" ] ; then 
		echo
	else
		echo "$file_metabo File not found"
		echo "File contained in the current folder: "
		find . -type f -name "*.xml"
		read -e file_metabo
		 
	fi
	if [ -e "$file_constraints" ] && [ "${file_constraints##*.}" == "txt" ] ; then 
		echo 
	else
		echo "$file_constraints File not found or have not the right extension"
		echo "File contained in the current folder: "
		find . -type f -name "*.txt"
		read -e file_constraints
		 
	fi

	if [ -e "$file_regu" ] && [ "${file_regu##*.}" == "xml" ] || [ "${file_regu##*.}" == "sbml" ] ; then 
		echo
	else
		echo "$file_regu File not found or have not the right extension"
		echo "File contained in the current folder: "
		find . -type f -name "*.xml"
		read -e file_regu
		
	fi

}


function verification_caspo(){
	if [ -e "$file_pkn" ] && [ "${file_pkn##*.}" == "sif" ]; then 
		echo 
	else
		echo "$file_pkn File not found"
		echo "File contained in the current folder : "
		find . -type f -name "*.sif"
		read -e file_pkn
	fi
	if [ -e "$file_dnf" ] ; then
	    echo 
	else 
	    echo "$file_dnf File not found or have not the right extension "
	    echo "File contained in the current folder : "
	    find . -type f -name "*dnf*"
	    read -e file_dnf
	fi  
	if [ -e "$file_setup" ] && [ "${file_setup##*.}" == "json" ] ; then
	    echo
	else
	    echo "$file_setup File not found or have not the right extension"
	    echo "File contained in the current folder : "
	    find . -type f -name "*.json"
	    read -e file_setup
	fi
}

function verification_global(){
	verifications_flexflux $file_metabo $file_constraints  $file_regu 
	verification_caspo $file_midas $file_pkn $file_setup $file_dnf
}

function creationDirectory(){
	#Enter the run number :
	read -e -p "Chose a run string or number which would be attribuate to the current run: " run_number
	if [ -d "result" ]; then
		echo "" 
		echo "Directory result/ already created previously"
		echo ""
		cd result/
		if [ -d "$run_number" ] ; then
			#echo "Run results present in the directory: "
			#pwd
			cd "$run_number"/
			if [ -d "R" ] ; then
				echo
				#echo "R scripts results present in the directory : "
				#pwd
			else 
				mkdir R/
			fi
			if [ -d "FlexFlux" ] ; then
				echo
				#echo "FlexFlux results present in the directory : "
				#pwd
			else
				mkdir FlexFlux/
			fi
			if  [ -d "Caspo" ]; then
				echo
				#echo "Caspo results present in the directory : "
				#pwd
			else 
				mkdir Caspo
			fi
		else
			#mkdir "$run_number"/
			mkdir -p "$run_number"/FlexFlux/
			mkdir -p "$run_number"/R/
			mkdir -p "$run_number"/Caspo/

		fi

	else
		echo "Creation of repository result/" 
		
		mkdir -p result/"$run_number"/FlexFlux/
		mkdir -p result/"$run_number"/R/
		mkdir -p result/"$run_number"/Caspo/
	fi
	cd $dir

}


function flexflux(){
	echo "You use FlexFlux"
	echo "Analyse option:"  >&2
	echo
	echo "		1.TDRFBA (time dependant FBA analyse)"
	echo "		2.RSA (regulatory steady state FBA analyse)"
	echo
	read -e -p "Choice of analyse: " analyse
	if [ "$analyse" == 1 ] || [ "$analyse" ==" TDRFBA" ] || [ "$analyse" == "tdrfba" ]; then
		echo
		echo "You choose time dependant FBA analyse"
		echo
		read -e -p "Name of the biomass reaction: " bio
		read -e -p "Add other options: " suppl
		echo
		bash $run/FlexFlux/Flexflux.sh TDRFBA -s $file_metabo -cons $file_constraints -reg $file_regu -bio $bio $suppl -x 0.01 -out out_times_"$run_number".csv -sol CPLEX 
		pip3 install matplotlib
		python3 $run/graph_panda_flexflux.py out_times_"$run_number".csv 
		mv out_times_$run_number.csv $dir/result/$run_number/FlexFlux/
		mv output_graph_FlexFlux.png $dir/result/$run_number/FlexFlux/
		flexflux_dir="$dir/result/$run_number/FlexFlux/"
		echo ""
		echo ""
		echo "File created:" ; ls $flexflux_dir
		echo ""
		flex_result="$dir/result/$run_number/FlexFlux/out_times_"$run_number".csv"
		scripts_R $flex_result $run
	else 
		echo
		echo "You choose regulatory steady state FBA analyse"
		echo	
		bash $run/FlexFlux/Flexflux.sh RSA -reg $file_regu -out out_RSA_"$run_number".csv 
		bash $run/FlexFlux/Flexflux.sh RSA -reg $file_regu -cons $file_constraints -out out_RSA_with_constrainst_"$run_number".csv 
		mv out_RSA_$run_number.csv $dir/result/$run_number/FlexFlux/
		mv out_RSA_with_constrainst_$run_number.csv $dir/result/$run_number/FlexFlux/
		flexflux_dir="$dir/result/$run_number/FlexFlux/"
		echo ""
		echo ""
		echo "File created:" ; ls $flexflux_dir
		echo ""
	fi
	
	
}

function scripts_R(){
	cd $run/scripts_R
	Rscript CreateMidas.R $flex_result
	Rscript FilterTimeSeries.R Midas.csv
	Rscript Discretize.R Filtered.csv 0.1
	Rscript CurveProfile.R Filtered.csv
	mv Midas.csv $dir/result/$run_number/R/
	mv Filtered.csv $dir/result/$run_number/R/
	mv Discretize.csv $dir/result/$run_number/R/
	mv CurveProfile.csv $dir/result/$run_number/R/
	cd $dir
	echo ""
	echo "File created: " ; ls result/$run_number/R/
	echo ""
	cd $dir
	cd result/$run_number/R
	R=$(pwd)
	cd $dir
}
function caspo() {
	echo "You use Caspo"
	#read -e -p "DA to analyse:" da
	#read -e -p "fit to analyse:" fit
	cd $run/caspo/
	#pip install caspo
	#Loading last docker version
	docker pull bioasp/caspo
	docker pull pauleve/caspots

	#Creation of directory use by the docker:
	if [ -d "caspo-wd" ]; then
		cd caspo-wd 
		cp $dir/$file_pkn ./pkn.sif
		cp $dir/$file_setup ./setup.json
		cp $dir/$file_dnf ./dnf.txt
		if [ -e "$dir/result/$run_number/R/Midas.csv" ] ; then 
			cp $dir/result/$run_number/R/Midas.csv	./midas_r.csv
		else
			cd $dir
			read -e -p "Midas file (.csv):		" file_midas
			cd $run/caspo/caspo-wd/
			cp $dir/$file_midas ./midas_r.csv
		fi
		if [ -e "$dir/result/$run_number/R/Discretize.csv" ] ; then 
			cp $dir/result/$run_number/R/Discretize.csv	./discret.csv
		else 
			cd $dir 
			read -e -p "Discretize file (.csv):		" file_discret
			cd $run/caspo/caspo-wd/
			cp $dir/$file_discret ./discret.csv
		fi

	else
		mkdir caspo-wd && cd caspo-wd
		cp $dir/$file_pkn ./pkn.sif
		cp $dir/$file_setup ./setup.json
	    cp $dir/$file_dnf ./dnf.txt
	    if [ -e "$dir/result/$run_number/R/Midas.csv" ] ; then 
			cp $dir/result/$run_number/R/Midas.csv	./midas_r.csv
		else 
			cd $dir 
			read -e -p "Midas file (.csv):		" file_midas
			cd $run/caspo/caspo-wd/
			cp $dir/$file_midas ./midas_r.csv
		fi
		if [ -e "$dir/result/$run_number/R/Discretize.csv" ] ; then 
			cp $dir/result/$run_number/R/Discretize.csv	./discret.csv
		else 
			cd $dir 
			read -e -p "Discretize file (.csv):		" file_discret
            cd $run/caspo/caspo-wd/
			cp $dir/$file_discret ./discret.csv
		fi
	fi
	echo
	echo
	echo "Option :"
	echo "1.Simple"
	echo "2.With control-nodes"
	echo "3.With control-nodes and partial-bn"
	echo "4.With partial-bn"
	echo "5.With partial-bn and family mincard"
	echo "6.With partial-bn and family mincard mincard-tolerance weight-tolerance"
	echo "7.With partial-bn and mincard-tolerance weight-tolerance"
	read -e -p "Choice of option: " option

	
	echo "----------------------------------------------------"
	echo "			Identify"
	echo "----------------------------------------------------"
	if [ "$option" == 1 ] ; then
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify pkn.sif discret.csv network_discret.csv 
	elif [ "$option" == 2 ] ; then
		echo "Give the control-nodes that you want to study separate by ,  (i.e. Carbon1,Carbon2) :"
		read control_nodes
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify pkn.sif discret.csv network_discret_nodes.csv --control-nodes "$control_nodes"
	elif [ "$option" == 3 ] ; then
		echo "Give the control-nodes that you want to study separate by ,  (i.e. Carbon1,Carbon2) :"
		read control_nodes
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify pkn.sif discret.csv network_discret_bn_nodes.csv --partial-bn dnf.txt --control-nodes "$control_nodes"
    elif [ "$option" == 4 ] ; then
        docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify     pkn.sif discret.csv network_discret_bn.csv --partial-bn dnf.txt 
	elif [ "$option" == 5 ] ; then
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify pkn.sif discret.csv network_discret_bn.csv --partial-bn dnf.txt --family mincard
	elif [ "$option" == 5 ] ; then
        docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify     pkn.sif discret.csv network_discret_bn.csv --partial-bn dnf.txt --family mincard --mincard-tolerance 0 --weight-tolerance 0 
    else
        docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots identify     pkn.sif discret.csv network_discret_bn.csv --partial-bn dnf.txt --mincard-tolerance 0 --weight-tolerance 0 

#--limit 10 --no-fully-controllable
	fi
	echo "----------------------------------------------------"
	echo "			Validate"
	echo "----------------------------------------------------"s
	if [ "$option" == 1 ] ; then
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots validate pkn.sif  discret.csv network_discret.csv --output network_validate.csv 
	elif [ "$option" == 2 ] ; then
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots validate pkn.sif  discret.csv network_discret_nodes.csv --output network_validate.csv --control-nodes "$control_nodes"
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots mse pkn.sif  discret.csv  --control-nodes "$control_nodes" 

	elif [ "$option" == 3 ] ; then
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots validate pkn.sif  discret.csv network_discret_bn_nodes.csv --output network_validate.csv --control-nodes "$control_nodes"
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots mse pkn.sif  discret.csv  --control-nodes "$control_nodes" --partial-bn dnf.txt
    else
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots validate pkn.sif  discret.csv network_discret_bn.csv --output network_validate.csv 
		docker run --rm --volume "$PWD":/wd --workdir /wd pauleve/caspots mse pkn.sif  discret.csv  --partial-bn dnf.txt 
	fi
nblines=$(wc -l network_validate.csv | sed 's/ network_validate.csv//') 
nbnet=$(($nblines-1))


	echo "----------------------------------------------------"
	echo "			Visualize"
	echo "----------------------------------------------------"
	if [ "$option" == 1 ] ; then
		docker run --rm -v $PWD:/caspo-wd -w /caspo-wd bioasp/caspo visualize --pkn pkn.sif --setup setup.json --networks network_discret.csv --midas midas_r.csv 0
	elif [ "$option" == 2 ] ; then 
		docker run --rm -v $PWD:/caspo-wd -w /caspo-wd bioasp/caspo visualize --pkn pkn.sif --setup setup.json --networks network_discret_nodes.csv --midas midas_r.csv 0
	elif [ "$option" == 3 ] ; then
		docker run --rm -v $PWD:/caspo-wd -w /caspo-wd bioasp/caspo visualize --pkn pkn.sif --setup setup.json --networks network_discret_bn_nodes.csv --midas midas_r.csv 0
	else
		docker run --rm -v $PWD:/caspo-wd -w /caspo-wd bioasp/caspo visualize --pkn pkn.sif --setup setup.json --networks network_discret_bn.csv --sample $nbnet
 
	fi

nbnets=$(($nbnet-1))
cd out/
for (( i=0; i<=$nbnets; i++ )) ; do
    dot -Tpng network-"$i".dot -o network-"$i".png 
done
dot -Tpng pkn.dot -o pkn.png
dot -Tpng networks-union.dot -o networks-union.png
cd ../

    echo "----------------------------------------------------"
	echo "			END"
	echo "----------------------------------------------------"
	mkdir files
	mv pkn.sif files/
	mv dnf.txt files/
	mv setup.json files/
	mv discret.csv files/
	mv midas_r.csv files/
	mv files/ ../
	cd ../
	ls $dir/result/
	echo "----------------------------------------------------"
	if [ -d "$dir/result/$run_number/Caspo/caspo-wd" ] && [ -d "$dir/result/$run_number/Caspo/files/" ]; then
		rm -rf $dir/result/$run_number/Caspo/*
		mv caspo-wd/ $dir/result/$run_number/Caspo/
		mv files/ $dir/result/$run_number/Caspo/
	else 
		mv caspo-wd/ $dir/result/$run_number/Caspo/
		mv files/ $dir/result/$run_number/Caspo/
	fi
	cd $dir 
}
function flexflux_analysis(){
	read_files_flexflux
	verifications_flexflux $file_metabo $file_constraints  $file_regu 
	creationDirectory 
	flexflux $file_metabo $file_constraints $file_regu $run_number $run
	
}
function caspo_analysis() {
	read_files_caspo
	verification_caspo $file_midas $file_pkn $file_setup $file_dnf
	creationDirectory 
	caspo $file_midas $file_pkn $file_setup $file_dnf $run

}
function basic_analysis () {
	read_files_flexflux
	read_files_caspo
	verification_global  $file_metabo $file_constraints  $file_regu  $file_midas $file_pkn $file_setup $file_dnf 
	creationDirectory 
	flexflux $file_metabo $file_constraints $file_regu $run_number $run
	caspo $file_midas $file_pkn $file_setup $file_dnf $run

}

function main(){
	#echo "Current location:"
	#pwd
	dir=$(pwd)
	cd src/ 
	run=$(pwd)
	cd $dir
	display
	case $choice in 
		BA | ba | --basic_analysis | 1 ) 
		basic_analysis $dir $run 
		exit 1
		;;
		F | f  | --flexflux | 2 )
		flexflux_analysis $dir $run
		exit 1
		;;
		C | c | --caspo  | 3 )
		caspo_analysis $dir $run
		exit 1
		;;
		H | h | help | 4 )
		display_help 
		break
		;;
    esac
}
main
