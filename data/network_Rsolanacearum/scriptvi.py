import libsbml
import sys
import re

#gen = sys.argv[1]
sbml_file = sys.argv[1]
reader = libsbml.SBMLReader()
document = reader.readSBML(sbml_file)
model = document.getModel()

listReactions = model.getListOfReactions()

reactant = {}
product = {}
notereaction = {}
for i in listReactions :
    reactant[i.id] = i.getListOfReactants()
    product[i.id] = i.getListOfProducts()
    notereaction[i.id] = i.getNotesString()

for k,v in notereaction.items():
    v = v.split('<html:p>')[1]
    v = re.search('(.*)', v).group(1)
    print(v)

