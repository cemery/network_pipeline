import sys
import libsbml

sbml_file = sys.argv[1]
idx_reac = sys.argv[2]


reader = libsbml.SBMLReader()
document = reader.readSBML(sbml_file)
model = document.getModel()

listReactions = model.getListOfReactions()

reac = listReactions.get(idx_reac)

l_prod = reac.getListOfReactants()

for i in l_prod:
    print(i.getSpecies())

