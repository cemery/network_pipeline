#!/usr/bin/env python
import sys
import libsbml

sbml_file = sys.argv[1]

reader = libsbml.SBMLReader()
document = reader.readSBML(sbml_file)
model = document.getModel()
listSpecies=model.getListOfSpecies()
listReactions=model.getListOfReactions()

def get_species(listSpecies):
	d_Species={}
	for i in listSpecies :
		d_Species[i.getId()]={	"name" : i.getName(), 
								"boundary" : i.getBoundaryCondition(),
								"constant" : i.getConstant(),
								"compartment" : i.getCompartment(),
								"sboTerm": i.getSBOTerm()
							}
	return d_Species


def get_reactions(listReactions):
	d_Reactions={}
	reac=[]
	prod=[]
	for i in listReactions :
		d_Reactions[i.getId()]={"name" : i.getName(), 
								"reversible" : i.getReversible(),
								"fast" : i.getFast(),
								#"lower" : i.getLowerFluxBound(),
								#"upper" : i.getUpperFluxBound(),
								"reac" : [[reac.getSpecies() for reac in i.getListOfReactants()], [reac.getStoichiometry() for reac in i.getListOfReactants()]],
								"prod" : [[prod.getSpecies() for prod in i.getListOfProducts()], [prod.getStoichiometry() for prod in i.getListOfProducts()]]	
							}
	return d_Reactions


def check(value, message):
	"""If 'value' is None, prints an error message constructed using
	'message' and then exits with status code 1.  If 'value' is an integer,
	it assumes it is a libSBML return status code.  If the code value is
	LIBSBML_OPERATION_SUCCESS, returns without further action; if it is not,
	prints an error message constructed using 'message' along with text from
	libSBML explaining the meaning of the code, and exits with status code 1.
	"""
	if value == None:
		raise SystemExit('LibSBML returned a null value trying to ' + message + '.')
	elif type(value) is int:
		if value == libsbml.LIBSBML_OPERATION_SUCCESS:
			return
		else:
			err_msg = 'Error encountered trying to ' + message + '.' \
					+ 'LibSBML returned error code ' + str(value) + ': "' \
					+ OperationReturnValue_toString(value).strip() + '"'
		raise SystemExit(err_msg)
	else:
		return




def create_model():



	try:
		document = libsbml.SBMLDocument(3, 1)
	except ValueError:
		raise SystemExit('Could not create SBMLDocumention object')

	#Name of the model : 
	model = document.createModel()
	check(model,                              'create model')
	check(model.setTimeUnits("second"),       'set model-wide time units')
	check(model.setExtentUnits("mole"),       'set model units of extent')
	check(model.setSubstanceUnits('mole'),    'set model substance units')
	check(model.setName("Mini R solanacearum to PhcA Proline Glutamine"),       'set model name')
	#Creation of unit (the same that was used by Aurelien)
	#TO SEE IF WE CONSERVED OR NOT CAUSE NOT IN THE DEFINITIF ONE
	mmol_per_gDW_per_hr = model.createUnitDefinition()
	check(mmol_per_gDW_per_hr,                         'create unit definition')
	check(mmol_per_gDW_per_hr.setId('mmol_per_gDW_per_hr'),     'set unit definition id')
	check(mmol_per_gDW_per_hr.setName("mmol_per_gDW_per_hr"),		'set name')
	unit_sec = mmol_per_gDW_per_hr.createUnit()
	check(unit_sec,                               'create unit on mmol_per_gDW_per_hr')
	check(unit_sec.setKind(libsbml.UNIT_KIND_SECOND),     'set unit kind')
	check(unit_sec.setExponent(-1),               'set unit exponent')
	check(unit_sec.setScale(0),                   'set unit scale')
	check(unit_sec.setMultiplier(0.00027778),              'set unit multiplier')
	unit_gram = mmol_per_gDW_per_hr.createUnit()
	check(unit_gram,                               'create unit on mmol_per_gDW_per_hr')
	check(unit_gram.setKind(libsbml.UNIT_KIND_GRAM),     'set unit kind')
	check(unit_gram.setExponent(-1),               'set unit exponent')
	check(unit_gram.setScale(0),                   'set unit scale')
	check(unit_gram.setMultiplier(1),              'set unit multiplier')
	unit = mmol_per_gDW_per_hr.createUnit()
	check(unit,                               'create unit on mmol_per_gDW_per_hr')
	check(unit.setKind(libsbml.UNIT_KIND_MOLE),     'set unit kind')
	check(unit.setExponent(1),               'set unit exponent')
	check(unit.setScale(-3),                   'set unit scale')
	check(unit.setMultiplier(1),              'set unit multiplier')
	#Create a compartment inside this model, and set the required
	# attributes for an SBML compartment in SBML Level 3.

	ext=model.createCompartment()
	check(ext,                                 'create compartment')
	check(ext.setId('ext'),                     'set compartment id')
	check(ext.setName('extracellular'),		  'set name')
	check(ext.setConstant(True),               'set compartment "constant"')
	cell = model.createCompartment()
	check(cell,                                'create compartment')
	check(cell.setId('cell'),                     'set compartment id')
	check(cell.setName('cell'),	       'set name')
	check(cell.setConstant(True),               'set compartment "constant"')


	# extra=model.createCompartment()
	# check(extra,                                 'create compartment')
	# check(extra.setId('e'),                     'set compartment id')
	# check(extra.setName('extracellular'),		  'set name')
	# check(extra.setConstant(True),               'set compartment "constant"')
	# # check(c1.setSize(1),                      'set compartment "size"')
	# # check(c1.setSpatialDimensions(3),         'set compartment dimensions')
	# # check(c1.setUnits('litre'),               'set compartment size units')
	# bound = model.createCompartment()
	# check(bound,                                'create compartment')
	# check(bound.setId('b'),                     'set compartment id')
	# check(bound.setName('boundary'),	       'set name')
	# check(bound.setConstant(True),               'set compartment "constant"')
	# # check(c1.setSize(1),                      'set compartment "size"')
	# # check(c1.setSpatialDimensions(3),         'set compartment dimensions')
	# # check(c1.setUnits('litre'),               'set compartment size units')

	# cyto = model.createCompartment()
	# check(cyto,                                'create compartment')
	# check(cyto.setId('c'),                     'set compartment id')
	# check(cyto.setName('cytoplasm'),	       'set name')
	# check(cyto.setConstant(True),               'set compartment "constant"')
	# # check(c1.setSize(1),                      'set compartment "size"')
	# # check(c1.setSpatialDimensions(3),         'set compartment dimensions')
	# # check(c1.setUnits('litre'),               'set compartment size units')
	# peri = model.createCompartment()
	# check(peri,                                'create compartment')
	# check(peri.setId('p'),                     'set compartment id')
	# check(peri.setName('periplasm'),	       'set name')
	# check(peri.setConstant(True),               'set compartment "constant"')
	# # check(c1.setSize(1),                      'set compartment "size"')
	# # check(c1.setSpatialDimensions(3),         'set compartment dimensions')
	# # check(c1.setUnits('litre'),               'set compartment size units')


	#List of parameters
	low= model.createParameter()
	check(low,									'create parameter k')
	check(low.setId('LOWER_BOUND_0'),			'set parameter k id')
	check(low.setConstant(True),				'set parameter k "constant"')
	check(low.setValue(0),						'set parameter k value')
	check(low.setUnits('mmol_per_gDW_per_hr'), 	'set parameter k units')
	upper= model.createParameter()
	check(upper,								'create parameter k')
	check(upper.setId('UPPER_BOUND_9999'),		'set parameter k id')
	check(upper.setConstant(True),				'set parameter k "constant"')
	check(upper.setValue(9999),					'set parameter k value')
	check(upper.setUnits('mmol_per_gDW_per_hr'),'set parameter k units')


	#create the species
	d_Species={}
	d_Species=get_species(listSpecies)
	for k, v in d_Species.items():
		spe= model.createSpecies()
		check(spe,												'create species spe')
		check(spe.setCompartment(v['compartment']),				'set species s1 compartment')
		check(spe.setConstant(v['boundary']),					'set "constant" attribute on s1')
		check(spe.setBoundaryCondition(v['boundary']),			'set "boundaryCondition" on s1')
		check(spe.setId(k),										'set species s1 id')
		check(spe.setName(v['name']),							'set species s1 id')
	d_Reactions={}
	d_Reactions=get_reactions(listReactions)
	#print (d_Reactions)
	for k, v in d_Reactions.items():
		#mplugin= model.getPlugin("fbc")
		reaction=model.createReaction()
		check(reaction,                                 		'create species spe')
		#check(reaction.setLowerFluxBound(v['lower']),			'set species s1 compartment')
		#check(reaction.setUpperFluxBound(v['upper']),			'set species s1 compartment')
		check(reaction.setReversible(v['reversible']),			'set "constant" attribute on s1')
		check(reaction.setFast(v['fast']),     					'set fast on s1')
		check(reaction.setId(k),                				'set species s1 id')
		check(reaction.setName(v['name']),						'set species s1 id')
		react=reaction.createReactant()
		check(react, 											'create reactant')
		if v['reac'][0]  : 
			for i in range (len (v['reac'][0])) :
				check(react.setSpecies(v['reac'][0][i]),		'assign reactant species')
				check(react.setStoichiometry(v['reac'][1][i]),	'set "stochio" ')
		prod=reaction.createProduct()
		check(prod, 											'create reactant')
		if v['prod'][0] :
			for j in range (len(v['prod'][0])) :			
				check(prod.setSpecies(v['prod'][0][j]),			'assign reactant species')
				check(prod.setStoichiometry(v['prod'][1][j]),	'set "stochio" ')


	return libsbml.writeSBMLToString(document)


if __name__ == '__main__':
	#create_model()
	print(create_model())
#use : python3 create_sbml.py input.sbml >output.xml 