#!/usr/bin/env python
'''
Created on 10 May. 2019

@author: Clara EMERY
@gitlab inria : https://gitlab.inria.fr/cemery
@git project : https://gitlab.inria.fr/cemery/network_pipeline
@git : Clara Emery (Arzhura)
'''

'''Import for librairies used '''
import sys
import libsbml
import pandas
import matplotlib
import matplotlib.pyplot as plt

''' Creation of a general script which import the others'''
###########################################################
import utils
import curration_csv
import create_meta
import create_regul



''' General link ''' 
