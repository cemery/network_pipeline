#!/usr/bin/env python
import sys
import libsbml

sbml_file = sys.argv[1]

reader = libsbml.SBMLReader()
document = reader.readSBML(sbml_file)
document.setPackageRequired('qual', True)
model = document.getModel()
mplugin = model.getPlugin("qual")
listCompartments=model.getListOfCompartments()
listSpecies=mplugin.getListOfQualitativeSpecies()
listTransitions=mplugin.getListOfTransitions()

# name=model.getName()
# model.setName('name')
# id_model=model.getId()
# model.setId('id_model')

def get_compart(listCompartments):
	d_Compartments={}
	for i in listCompartments :
		d_Compartments[i.getId()]={	"name" :i.getName(),
									"constant":i.getConstant(),
								}
	return d_Compartments

def get_species(listSpecies):
	d_Species_read={}
	for i in listSpecies :
		d_Species_read[i.getId()]={	"name" : i.getName(), 
								"compartment" : i.getCompartment(),
								"constant" : i.getConstant(),
								"sboTerm": i.getSBOTerm(),
								"initial" : i.getInitialLevel(),
								"maxlevel" : i.getMaxLevel(),
								"notes" : i.getNotes()								
							}
	return d_Species_read


def get_transitions(listTransitions):
	d_Transitions_read={}
	for i in listTransitions :
		#print ([i.getFunctionTerm(0).getMath()])
		d_Transitions_read[i.getId()]={	"inp" : [
											[inp.getSBOTerm() for inp in i.getListOfInputs()], 
											[inp.getId() for inp in i.getListOfInputs()],
											[inp.getQualitativeSpecies() for inp in i.getListOfInputs()],
											[inp.getTransitionEffect() for inp in i.getListOfInputs()],
											[inp.getSign() for inp in i.getListOfInputs()],
											],
									"out" : [
											[out.getId() for out in i.getListOfOutputs()],
											[out.getQualitativeSpecies() for out in i.getListOfOutputs()],
											[out.getTransitionEffect() for out in i.getListOfOutputs()],
											],
									"function":[
											[i.getFunctionTerm(0).getMath()],
											[i.getFunctionTerm(0).getResultLevel()]
											],
									"default":[
											[i.getListOfFunctionTerms().getDefaultTerm().getResultLevel()],
									]
							}
		
	return d_Transitions_read


def create_model():
	sbmlns = libsbml.SBMLNamespaces(3, 1,'qual', 1)
	document = libsbml.SBMLDocument(sbmlns)
	document.setPackageRequired('qual', True)
	# create the Model
	model=document.createModel()
	name=model.getName()
	model.setName('name')
	id_model=model.getId()
	model.setId('id_model')
	# Get a QualModelPlugin object plugged in the model object.
	mplugin = model.getPlugin("qual")
	# model.setName('Mini regulatory R solanacearum to PhcA Proline Glutamine')

	# create the Compartments
	d_Compartments={}
	d_Compartments=get_compart(listCompartments)
	for k, v in d_Compartments.items():
		c= model.createCompartment()
		c.setId(k)
		c.setName(v['name'])
		c.setConstant(v['constant'])
	
	# create the QualitativeSpecies
	d_Species={}
	d_Species=get_species(listSpecies)
	for k, v in d_Species.items():
		spe= mplugin.createQualitativeSpecies()
		spe.setId(k)
		spe.setName(v['name'])
		spe.setCompartment(v['compartment'])
		spe.setConstant(v['constant'])
		spe.setInitialLevel(v['initial'])
		spe.setMaxLevel(v['maxlevel'])
		spe.setNotes(v['notes'])	


	# create the Transitions
	d_Transitions={}
	d_Transitions=get_transitions(listTransitions)
	for k, v in d_Transitions.items():
		trans=mplugin.createTransition()
		trans.setId(k)
		inp=trans.createInput()
		if v['inp'][0]  : 
			for i in range (len (v['inp'][0])) :
				inp.setSBOTerm(v['inp'][0][i])
				inp.setId(v['inp'][1][i])
				inp.setQualitativeSpecies(v['inp'][2][i])
				inp.setTransitionEffect(v['inp'][3][i])
				inp.setSign(v['inp'][4][i])

		out=trans.createOutput()
		if v['out'][0] :
			for j in range (len(v['out'][0])) :			
				out.setId(v['out'][0][j])
				out.setQualitativeSpecies(v['out'][1][j])
				out.setTransitionEffect(v['out'][2][j])
		
		fct=trans.createFunctionTerm()
		if v['function'][0] :
			for m in range (len(v['function'][0])) :
				fct.setMath(v['function'][0][m])
				fct.setResultLevel(v['function'][1][m])
		dt=trans.createDefaultTerm()
		if v['default'][0] :
			for n in range (len(v['default'][0])) :
				dt.setResultLevel(v['default'][0][n])
	return libsbml.writeSBMLToString(document)


if __name__ == '__main__':
	#create_model()
	print(create_model())
#use : python3 create_sbml.py input.sbml >output.xml 