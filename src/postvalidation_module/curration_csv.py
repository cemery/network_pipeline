import csv
import sys

file_csv = sys.argv[1]

with open(file_csv, 'r') as csvfile:
	header = csvfile.readline()
	header = header.split(',')
	l_header = len(header)
	dict1 = {}
	dict2 = {}
	cnt = 0
	common=0
	for n in range(l_header):
		dict1[n] = 0 
		dict2[n] = 0				#intialization of dicotionnary to 0
	for i in csvfile:				#parcours la ligne dans le fichier		
		i = i.split(',')			#split les values de la ligne 
		for c in range(l_header):	#pour chq valeur dans la colonne de 0 à longueur du fichier
			dict1[c]+=int(i[c])		#ajout de la valeur dans le dico
			#print(dict1)
			if int(dict2[c])==1 :
				#print(dict2[c])
				print(header)
 		
		cnt += 1					#compte les lignes 
	for k, v in dict1.items():		#parcours du dico  de chacune des clé et valeur associées
		if v == cnt:				#si la valeur dans le dico est au max du nombre de valeur dans la colonne (1+1+1+... etc)
			print(header[k])		#alors le header est commun et on le print 
			common+=1	

	#print (common)

''' Pour chacune des lignes au dessus du nb de header
on peut afficher quelles sont les facteurs d'importance'''
	# for j in csvfile :
	# 	j = j.split(',')
	# 	for c in range(l_header) :
	# 		if dict2[c]== 1 :
	# 			print(header)

