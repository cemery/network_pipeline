----------------------
INSTRUCTIONS
----------------------

Les 3 environnements permettent d'utiliser respectivement Caspots, FlexFlux et Rstudio
Après installation d'Anaconda 3, utiliser la commande suivante pour reconstruire les différents environnements: conda env create -f "environment.yml"
Pour utiliser un environnement, utiliser la commande: . activate "environnement"
Pour changer d'environnement, désactiver d'abord le premier en utilisant la commande: . deactivate "environnement"

