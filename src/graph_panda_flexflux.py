#!/usr/bin/env python
import sys
import pandas
import matplotlib
import matplotlib.pyplot as plt
data=sys.argv[1]

df= pandas.read_csv(data, sep="\t")

loc=['center right']

time=df['Time']

bio=df['Biomass']

glu=df['Glutamine']

pro=df['Proline']

print(type(df))

plt.plot(time, bio, label='Biomass')
plt.plot(time, glu, label='Glutamine')
plt.plot(time, pro, label='Proline ')
plt.legend(loc=7) #on the right center

plt.xlabel('time')
plt.ylabel('concentrations')

plt.savefig('output_graph_FlexFlux.png')

